(function (module) {
    var catalogueService = function ($http, myConfig) {
        
        var getCatalogueType = function (catalogueType, UserId, profileName) {
            return $http.get(myConfig.apiUrl + '/api/Content/GetCatalogueByType?catalogueType=' +catalogueType+ '&UserId=' +UserId+ '&profileName=' + profileName )
                .then(function (response) {
                return response;
            });
        };
        
        var getCatalogue = function (id) {
            return $http.get(myConfig.apiUrl + '/api/Content/GetCatalogueById?catalogueId=' +id )
                .then(function (response) {
                return response;
            });
        };
        
         var addContent = function (catalogueId, contentId) {
            return $http.get(myConfig.apiUrl + '/api/Content/AddContentToCatalogue?catalogueId=' +catalogueId+ '&contentId=' +contentId )
                .success(function (data, status, header, config) {

                })
                .error(function (data, status, header, config) {
                    console.info('error');
                });
            };
        
        var removeContent = function (catalogueId, contentId) {
            return $http.get(myConfig.apiUrl + '/api/Content/RemoveContentFromCatalogue?catalogueId=' +catalogueId+ '&contentId=' +contentId)
                .success(function (data, status, header, config) {

                })
                .error(function (data, status, header, config) {
                    console.info('error');
                });
            };
		
		
		
		
		
		
		
		
		
        
        return {
            getCatalogueType: getCatalogueType,
            getCatalogue: getCatalogue,
            addContent: addContent,
            removeContent: removeContent
            }
    };

    module.factory("catalogueService", catalogueService);
}(angular.module("app")));