(function(module){
    module.service("categoryService",function($http,myConfig){

       this.getAllCategoryList = function(){
           return $http.get(myConfig.apiUrl + '/api/Category/Get')
           .then(function(success){
            return success.data;            
           });
       }; 
        
       this.getParticularCategory = function(id){
            return $http.get(myConfig.apiUrl + '/api/Category/Get?id=' +id)
           .then(function(success){
            return success.data;            
           });
           
       };
        
       this.updateParticularCategory = function(particularCategory){
        if(particularCategory.id==='' || particularCategory.id === undefined)
        {
           return $http.post(myConfig.apiUrl + '/api/Category/Post' , particularCategory)
           .then(function(success){
            return success.data;            
           });
        }
           else{
               return $http.post(myConfig.apiUrl + '/api/Category/Post' , particularCategory);
           }
       };
        
		
		this.updateParticularSubCategory = function(particularCategory){
        //if(particularCategory.id !=='' || particularCategory.id !== undefined)
        //{
               return $http.post(myConfig.apiUrl + '/api/Category/Post' , particularCategory);
          // }
       };
        
    
       this.deleteParticularCategory = function(id){
           return $http.delete(myConfig.apiUrl + '/api/Category/Delete?id=' + id)
           .then(function (success) {
                return success;
            });
       };
        
    });
    
}(angular.module("app")));