(function (module) {
    var contentService = function ($http, myConfig) {
        
         var getContent = function (id) {
            return $http.get(myConfig.apiUrl + '/api/content/get?id=' +id )
                .then(function (response) {
                return response;
            });
        };
        
          var deleteContent = function (id) {
            return $http.delete(myConfig.apiUrl + '/api/content/delete?id=' +id )
                .then(function (response) {
                return response;
            });
        };
        
        var getAllContent = function () {
            return $http.get(myConfig.apiUrl + '/api/content/get' )
                .then(function (response) {
                return response;
            });
        };
        
        var updateContent = function (contentData) {
            return $http.post(myConfig.apiUrl + '/api/content/post', contentData)
                .success(function (data, status, header, config) {

                })
                .error(function (data, status, header, config) {
                    console.info('error');
                });
        };
		
		
		var getProductOptions = function () {
            return $http.get(myConfig.apiUrl + '/api/account/getproducts')
                .then(function (response) {
                return response;
            });
        };
		
		/*var getAutocompleteText = function (startwith) {
            return $http.get(myConfig.apiUrl + '/api/actor/search?startsWith=' +startwith)
                .then(function (response) {
                return response;
            });
        };*/
        
		var getAutocompleteText = function (startwith) {
			console.log("startwith=",startwith);
            return $http.get(myConfig.apiUrl + '/api/actor/search?startsWith=' +startwith)
                .then(function (response) {
				
				var responseArr = [];
				
var len = response.data.length;
				
for (var i = 0; i < len; i++) {
    responseArr.push({
       ActorId: response.data[i].id,
        ActorName: response.data[i].name
    });
	
}
				console.log("responseArr",responseArr);
                return responseArr;
				
            });
        };
        /*var uploadImage = function (){
            return $http.post(myConfig.apiUrl + '/api/content/UpdateContentImage')
                .success(function (data, status, header, config) {

                })
                .error(function (data, status, header, config) {
                    console.info('error');
                });
        };*/
        
        return {
            getContent: getContent,
            deleteContent: deleteContent,
            getAllContent: getAllContent,
            updateContent: updateContent,
			getAutocompleteText: getAutocompleteText,
			getProductOptions: getProductOptions
            /*uploadImage: uploadImage*/
            }
    };

    /*var module= angular.module("app");*/
    module.factory("contentService", contentService);
}(angular.module("app")));


