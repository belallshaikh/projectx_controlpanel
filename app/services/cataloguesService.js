(function(module){
    module.service("cataloguesService",function($http,myConfig){

       this.getAllCataloguesList = function(){
           return $http.get(myConfig.apiUrl + '/api/Catalogue/Get')
           .then(function(success){
            return success.data;            
           });
       }; 
        
       this.getParticularCatalogue = function(id){
            return $http.get(myConfig.apiUrl + '/api/Catalogue/Get?id=' +id)
           .then(function(success){
            return success.data;            
           });
           
       };
        
       this.updateParticularCatalogue = function(particularCatalogue){
        if(particularCatalogue.id==='' || particularCatalogue.id === undefined)
        {
           return $http.post(myConfig.apiUrl + '/api/Catalogue/Post' , particularCatalogue)
           .then(function(success){
            return success.data;            
           });
        }
           else{
               return $http.post(myConfig.apiUrl + '/api/Catalogue/Post' , particularCatalogue);
           }
       };
        
    
       this.deleteParticularCatalogue = function(id){
           return $http.delete(myConfig.apiUrl + '/api/Catalogue/Delete?id=' + id)
           .then(function (success) {
                return success;
            });
       };
        
    });
    
}(angular.module("app")));