(function(module){
    module.service("castCrewService",function($http,myConfig){
         /*this.CCList=[
          {id:"1",
          name:"Akshay Kumar",
          profile: ['Actor'],
          proimg: "dist/img/cast crew/akshaykumar.jpg" ,
          movies: ['Khiladi','Aitraaz']
          },
          
          {id:"2",
          name:"Aamir Khan",
          profile: ['Actor','Producer'],
          proimg: "dist/img/cast crew/AamirKhan.jpg" ,
          movies: ['Taare Zameen Par','Andaaz Apna Apna']
          },
          
          {id:"3",
          name:"Priyanka Chopra",
          profile: ['Actress','Singer'],
          proimg: "dist/img/cast crew/priyankachopra.jpg" ,
          movies: ['Aitraaz','Mary Kom','Dil Dhadakane Do']
          },
          
          {id:"4",
          name:"Brad Pitt",
          profile: ['Actor','Producer'],
          proimg: "dist/img/cast crew/BradPitt.jpg" ,
          movies: ['Troy','Mr & Mrs Smith']
          }
             
      ];*/
        
       this.getCCAllDetailsList = function(){
           return $http.get(myConfig.apiUrl + '/api/Actor/Get')
           .then(function(success){
            return success.data;            
           });
       }; 
        
       this.getParticularCC = function(id){
            return $http.get(myConfig.apiUrl + '/api/Actor/Get?id=' +id)
           .then(function(success){
            return success.data;            
           });
           
       };
        
       this.updateParticularCC = function(particularCC){
        if(particularCC.id==='' || particularCC.id === undefined)
        {
           return $http.post(myConfig.apiUrl + '/api/Actor/Post' , particularCC)
           .then(function(success){
            return success.data;            
           });
        }
           else{
               return $http.post(myConfig.apiUrl + '/api/Actor/Post' , particularCC);
           }
       };
        
    
       this.deleteParticularCC = function(id){
           return $http.delete(myConfig.apiUrl + '/api/Actor/Delete?id=' + id)
           .then(function (success) {
                return success;
            });
       };
        
    });
    
}(angular.module("app")));