(function(module){
    
    module.service("accountService",function($http,myConfig){
       

		 this.getAccountsList = function(searchCondition){
			console.log("searchCondition=",searchCondition);
           return $http.post(myConfig.apiUrl + '/api/Account/Search',searchCondition)
           .then(function(success){
			   console.log("success.data=",success.data);
            return success.data;            
           });
       }; 
        
        
       this.getAccount = function(id){
            return $http.get(myConfig.apiUrl + '/api/Account/GetAccount?id=' +id)
           .then(function(success){
            return success.data;            
           });
           
       };
		
		this.makePayout=function(data){
			return $http.post(myConfig.apiUrl+'/api/Payment/makePayout',data)
			.then(function(response){return response;});
			
		};
        this.getAccountProperties=function(id){
			return $http.get(myConfig.apiUrl+'/api/Property/GetAccountProperties?id='+id);
			
		}
		
       /*this.updateParticularAccount = function(particularAccount){
        if(particularAccount.id==='' || particularAccount.id === undefined)
        {
           return $http.post(myConfig.apiUrl + '/api/Actor/Post' , particularAccount)
           .then(function(success){
            return success.data;            
           });
        }
           else{
               return $http.post(myConfig.apiUrl + '/api/Actor/Post' , particularAccount);
           }
       };
        
    
       this.deleteParticularAccount = function(id){
           return $http.delete(myConfig.apiUrl + '/api/Actor/Delete?id=' + id)
           .then(function (success) {
                return success;
            });
       };*/
        
    });
}(angular.module("app")));