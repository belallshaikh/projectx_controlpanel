angular
    .module('app')
/*=================multiselect dropdown list with name===============================*/
    .directive('dropdownMultiselect', function(){
    return {
                restrict: 'E',
                scope: {
                    model: '=',
                    options: '=',
                },
                template:
                        "<div class='form-control' data-ng-class='{open: open}'>" +
                            "<div class=' dropdown-toggle' data-ng-click='openDropdown()' style='cursor:pointer;'>{{text}}"+
                            "<span class='caret pull-right'></span></div>" +
                            "<ul class='dropdown-menu multiselect-dropdown-menu' aria-labelledby='dropdownMenu'>" +
                                "<li><a data-ng-click='selectAll()'>"+
                                "<span class='glyphicon glyphicon-ok green'aria-hidden='true'></span> Check All</a></li>" +
                                "<li><a data-ng-click='deselectAll();'>"+
                                "<span class='glyphicon glyphicon-remove red' aria-hidden='true'></span> Uncheck All</a></li>" +
                                "<li class='divider'></li>" +
                                "<li data-ng-repeat='option in options'>"+
                                "<a data-ng-click='toggleSelectItem(option)'>"+
                                "<span data-ng-class='getClassName(option)' aria-hidden='true'></span> {{option}}</a></li>" +
                            "</ul>" +
							 "</div>",
//                        "</div>" +
//		 					"<br>" +
//            			"<input type=text readonly data-ng-model='items'>",

                controller: function ($scope) {
					$scope.text="Select";
                    $scope.openDropdown = function () {
                        $scope.open = !$scope.open;
                    };

                    $scope.selectAll = function () {
                        $scope.model = [];
                        angular.forEach($scope.options, function (item, index) {
                            $scope.model.push(item);
							//console.log("$scope.model",$scope.model);
                        });
                    };

                    $scope.deselectAll = function () {
                        $scope.model = [];
						$scope.text="Select";
                    };

                    $scope.toggleSelectItem = function (option) {
                        var intIndex = -1;
                        angular.forEach($scope.model, function (item, index) {
                            if (item == option) {
                                intIndex = index;
                            }
                        });

                        if (intIndex >= 0) {
                            $scope.model.splice(intIndex, 1);
                        }
                        else {
                            $scope.model.push(option);
                        }
                    };

                    $scope.getClassName = function (option) {
                        var varClassName = 'glyphicon glyphicon-remove red';
                        angular.forEach($scope.model, function (item, index) {
							 if ($scope.model.length > 2) {
								 $scope.slice=[];
                                 $scope.slice = $scope.model.slice(0, 2);
								 $scope.join=$scope.slice.join(', ') + '...';
								 $scope.text=$scope.join;
                            } else {
								$scope.text=$scope.model.join(', ');
							}
                            if (item == option) {
								//console.log("item=",item);
								//console.log("option=",option);
                                varClassName = 'glyphicon glyphicon-ok green';
                            }
                        });
                        return (varClassName);
                    };
                }
            }
        })

/*=================  multiselect dropdown list with Id and name ===============================*/
.directive('dropdownMultiselectWithId', function(){
    return {
                restrict: 'E',
                scope: {
                    model: '=',
                    options: '=',
                },
                template:
                        "<div class='form-control' data-ng-class='{open: open}'>" +
                            "<div class=' dropdown-toggle' data-ng-click='openDropdown()' style='cursor:pointer;'>{{text}}"+
                            "<span class='caret pull-right'></span></div>" +
                            "<ul class='dropdown-menu multiselect-dropdown-menu' aria-labelledby='dropdownMenu'>" +
                                "<li><a data-ng-click='selectAll()'>"+
                                "<span class='glyphicon glyphicon-ok green'aria-hidden='true'></span> Check All</a></li>" +
                                "<li><a data-ng-click='deselectAll();'>"+
                                "<span class='glyphicon glyphicon-remove red' aria-hidden='true'></span> Uncheck All</a></li>" +
                                "<li class='divider'></li>" +
                                "<li data-ng-repeat='option in options'>"+
                                "<a data-ng-click='toggleSelectItem(option)'>"+
                                "<span data-ng-class='getClassName(option)' aria-hidden='true'></span> {{option.name}}</a></li>" +
                            "</ul>" +
                        "</div>",

                controller: function ($scope) {
                    $scope.openDropdown = function () {
						$scope.text="Select";
                        $scope.open = !$scope.open;
                    };

/*                    $scope.selectAll = function () {
                        $scope.model = [];
                        angular.forEach($scope.options, function (item, index) {
                            $scope.model.push({
                                           id: item.id,
                                           name: item.name
                                       });
                        });
                    };

                    $scope.deselectAll = function () {
                        $scope.model = [];
                    };

                    $scope.toggleSelectItem = function (option) {
                        var intIndex = -1;
                        angular.forEach($scope.model, function (item, index) {
                            if (item == option.id) {
                                intIndex = index;
                            }
                        });

                        if (intIndex >= 0) {
                            $scope.model.splice(intIndex, 1);
                        }
                        else {
                            $scope.model.push({
                                           id: item.id,
                                           name: item.name
                                       });
                        }
                    };

                    $scope.getClassName = function (option) {
                        var varClassName = 'glyphicon glyphicon-remove red';
                        angular.forEach($scope.model, function (item, index) {
                            if (item == option.id) {
                                varClassName = 'glyphicon glyphicon-ok green';
                            }
                        });
                        return (varClassName);
                    };*/
					
					$scope.selectAll = function () {
                        $scope.model = [];
                        angular.forEach($scope.options, function (item, index) {
							//console.log("item=",item);
                            $scope.model.push(item);
							//console.log("$scope.model",$scope.model);
                        });
                    };

                    $scope.deselectAll = function () {
                        $scope.model = [];
						$scope.text="Select";
                    };

                    $scope.toggleSelectItem = function (option) {
                        var intIndex = -1;
                        angular.forEach($scope.model, function (item, index) {
                            if (item.id == option.id) {
                                intIndex = index;
                            }
                        });

                        if (intIndex >= 0) {
                            $scope.model.splice(intIndex, 1);
                        }
                        else {
                            $scope.model.push(option);
                        }
                    };

                    $scope.getClassName = function (option) {
						//console.log("option=",option);
//                        var varClassName;
//                        angular.forEach($scope.model, function (item, index) {
//                            if (item == option) {
//								console.log("item=",item);
//								console.log("option=",option);
//                                varClassName = 'glyphicon glyphicon-ok green';
//                            }
//							else
//							{
//							   varClassName = 'glyphicon glyphicon-remove red';
//							}
//                        });
  //                      return (varClassName);
						var varClassName = 'glyphicon glyphicon-remove red';
                        angular.forEach($scope.model, function (item, index) {
							//console.log("$scope.model=",$scope.model);
							if($scope.model.length > 1 && $scope.model.length <3){
								$scope.text=$scope.model[0].name + ", "+ $scope.model[1].name;
							} else if ($scope.model.length >= 3){
								$scope.text=$scope.model[0].name + ", "+ $scope.model[1].name + "...";
							} else {
								$scope.text=$scope.model[0].name;
							}
							
							
						//console.log("item outside if=",item);
                            if (item.id == option.id) {
									//console.log("item.id=",item.id);
								//console.log("option.id=",option.id);
								//console.log("option.name=",option.name);
                                varClassName = 'glyphicon glyphicon-ok green';
                            } else {
							//console.log("item & option do no match...");
							}
                        });
                        return (varClassName);
                    };
                
					
                }
            }
        })


				/*====================For number only fields*==============================*/
.directive('numbersOnly', function(){
   return {
     require: 'ngModel',
     link: function(scope, element, attrs, modelCtrl) {
       modelCtrl.$parsers.push(function (inputValue) {
           // this next if is necessary for when using ng-required on your input. 
           // In such cases, when a letter is typed first, this parser will be called
           // again, and the 2nd time, the value will be undefined
           if (inputValue == undefined) return '' 
           var transformedInput = inputValue.replace(/[^0-9]/g, ''); 
           if (transformedInput!=inputValue) {
              modelCtrl.$setViewValue(transformedInput);
              modelCtrl.$render();
           }         

           return transformedInput;         
       });
     }
   };
});


