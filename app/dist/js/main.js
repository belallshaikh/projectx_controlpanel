 angular
     .module("app", ['ngAnimate',
              'toaster',
              'ngCookies',
              'ngResource',
              'ngRoute',
              'ngSanitize',
              'ngTouch',
              'afOAuth2',
              'ngMessages',
              'chieffancypants.loadingBar',
              'ngFileUpload',
              'angularUtils.directives.dirPagination',
                     'angularjs-autocomplete',
					 'mwl.confirm', 'ui.bootstrap.position',
					 'angularjs-dropdown-multiselect',
					 'ngTagsInput',
					 'angular-uuid'
				//'720kb.datepicker'
			  //'MassAutoComplete'
             ])

 .config(
     function ($routeProvider, $locationProvider) {
      //   $locationProvider.html5Mode(true);

         $routeProvider
                     
             .when('/AccountsList', {
                 templateUrl: 'views/accountsList.html',
                 requireToken: true
             })
		 
		 .when('/AccountDetails/:id', {
                 templateUrl: 'views/accountDetails.html',
                 requireToken: true
             })
		 
		 
		 
          .when('/PropertiesList', {
                 templateUrl: 'views/propertiesList.html',
                 requireToken: true
             })
         
		 .when('/PropertyDetails/:id', {
                 templateUrl: 'views/propertyDetails.html',
                 requireToken: true
             })
        
			 
			 
			 .when('/TripsList', {
                 templateUrl: 'views/tripsList.html',
                 requireToken: true
             })
         
		 
		 .when('/TripDetails/:id', {
                 templateUrl: 'views/tripDetails.html',
                 requireToken: true
             })
		 
		 
         .when('/BookingsList', {
                 templateUrl: 'views/bookingsList.html',
                 requireToken: true
             })
		 
		 .when('/BookingDetails/:id', {
                 templateUrl: 'views/bookingDetails.html',
                 requireToken: true
             })
		 
         .when('/TransactionsList', {
                 templateUrl: 'views/transactionsList.html',
                 requireToken: true
             })

            .when('/TransactionDetails/:id', {
                 templateUrl: 'views/transactionDetails.html',
                 requireToken: true

             })
		 
		 
		 .when('/login', {
                 templateUrl: 'views/login.html'
             })
		 
		 
             .when('/contentMetaEdit/:id', {
                 templateUrl: 'views/contentMetadataEdit.html',
                 requireToken: true

             })
                  
             .when('/contentMetadataView/:id', {
                 templateUrl: 'views/contentMetadataView.html',
                 requireToken: true

             })
            
             
		 
			 .when('/dashboard', {
                 templateUrl: 'views/dashboard.html',
                 requireToken: true
             })
             .otherwise({
                 redirectTo: '/dashboard'
             });
     })


 .constant("myConfig", {

     //Live PROD
//     "apiUrl": "http://projectxwebapi.azurewebsites.net"

     //QA
     //"apiUrl": "http://omgapi_qa.azurewebsites.net"

     //Local
     "apiUrl": "http://localhost:57685"
 })

 .controller("MainController", function () {
     var main = this;
     main.tab = 1;
     main.selectTab = function (selecttab) {
         main.tab = selecttab;
     };
     main.isSelected = function (isselected) {
         return main.tab === isselected;

     };

 });