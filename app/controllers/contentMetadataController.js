/*'use strict'*/
/*angular.module('app').controller('contentMetadataController',
             function contentMetadataController($scope,$location){*/

(function (module) {

    var contentMetadataController = function (contentService, $location, $routeParams, toaster, cfpLoadingBar, Upload, myConfig, $timeout, castCrewService,$http, $q) {
        var vm = this;

        vm.currentContentId = $routeParams.id;
        
        vm.genreOptions = ['Action', 'Adventure', 'Comedy', 'Drama', 'Crime', 'Erotica', 'Fiction', 'Fantasy', 'Historical', 'Horror', 'Mystery', 'Paranoid', 'Philosophical', 'Political', 'Realistic', 'Romance', 'Saga', 'Satire', 'Sciencefiction'];
        
        vm.languagesOptions = ['Hindi', 'Tamil', 'Telugu', 'Marathi', 'Punjabi', 'Gujarati', 'Bangla', 'English', 'Bhojpuri', 'Malayalam', 'Assamese', 'Bihari', 'Kannada', 'Other'];
        
        vm.regionallangOptions = [ 'Hindi', 'Tamil', 'Telugu', 'Marathi', 'Punjabi', 'Gujarati', 'Bangla', 'English', 'Bhojpuri', 'Malayalam', 'Assamese', 'Bihari', 'Kannada', 'Other'];
        
        vm.targetAudienceOptions =['Male', 'Female', 'Children'];
        
        vm.targetRegionOptions =['Europe', 'North America', 'Middle East', 'Asia', 'MSouth east Asia', 'Africa'];
        
        vm.distRightsOwnerOptions =['option1', 'option2', 'option3'];
        
        vm.omgProductListOptions =[];

        vm.content = {
            exclusive: "", //Exclusive
            name: "", //content Name
            uploadedOn: "", //uploaded on date
            contentType: "", //content type
            genre: [], //genre
            aka: "", //content alias
            filmingLocation: "", //filming loaction
            languages: [], //language
            regionallang: [], //regional language 
            distributor: "", //content distributor 
            producer: [
                {
                    ActorId:"",
                    ActorName:""
                }
            ], //content producer 
            director: [ {
                    ActorId:"",
                    ActorName:""
                }], //director 
            writer: [ {
                    ActorId:"",
                    ActorName:""
                }], //writer 
            music: [ {
                    ActorId:"",
                    ActorName:""
                }], //music director 
            lyrics: [ {
                    ActorId:"",
                    ActorName:""
                }], //lyrics by 
			
			Tags: [{
				Text:""
			}], //tags
            site: "", //official site 
            launchMonthYear: "", //launch month year 
            subTitlesAvailable: "", //subtitle available 
            trailerAvailable: "", //trailer available 
            thumbnailImage: "", //thumbnail image 
            posterImage: "", //poster image 
            trailercontent: "", //trailer 
            publicityMedia: "", //publicity media 
            targetRegion: [], //target region 
            targetAudience: [], //target audience 
            mainstream: "", //main stream or regional 
            keywords: "", //storyline keyword 
            cast: [ {
                    ActorId:"",
                    ActorName:""
                }], //star cast 
            story: "", //story line 
            trivia: "", //trivia 
            quotes: "", //quotes 
            formt: "", //content format 
            resolution: "", //content resolution 
            duration: "", //content duration 
            color: "", //color 
            quality: "", //content quality 
            soundQuality: "", //sound quality 
            vBitRate: "", //video bit rate 
            aBitRate: "", //audio bit rate 
            aspectRatio: "", //aspect ratio 
            cbandwidth: "", //Content Bit Rate/Bandwidth 
            codec: "", //codec format 
            camera: "", //camera 
            printedFilmFormat: "", //printed film format 
            negativeFormat: "", //negative format 
            cinematographicProcess: "", //cinematographic process 
            boxOffice: "", //boxoffice performance 
            topGrosser: "", //top grosser 
            rating: 0, //industry rating 
            omgRating: 0, //omg rating 
            customerRating: 0, //customer rating 
            awards: "", //awards 
            feaindicator: "", //feature indicator 
            CertIN: "", //certificate india 
            ClassificationUK: "", //classification Uk 
            countryOfOrigin: "", //county of origin 
            copyright: "", //copy right protected 
            distRightsAcq: "", //Distribution Rights Acquired 
            distRightsOwner: [], //Distribution Rights Owner 
            omgProductList: [
                {
                    id: "",
                    name: "",
                    amount: "",
                    currency: "",
                    
                }
                
			], //select product 
            pubFrom: "", //publication date 
            pubTill: "", //expiration date 
            cost: [
                {
                    type: "", //purchase type 
                    amount: "", //amount 
                    from: "", //from date 
                    till: "" //till date
                }
            ],
            pubUrl: "", //PublishUrl
            id: "",
            dType: "", //content
            desc: "",
            CreatedBy: "",
            UpdatedBy: "",
            CreatedOn: "",
            UpdatedOn: "",
            Active: ""
        };

        vm.ContentList = [];

     /*-----------function for adding new row into (content amount details) table-----------*/
        vm.addItem = function () {
            vm.content.cost.push({
                type: "", //purchase type 
                amount: "", //amount 
                from: "", //from date 
                till: "" //till date
            });
        };
		
	/*----------------------------------------------------------------------*/

        vm.gotoContentMetadataView = function (id) {
            $location.path('/contentMetadataView/' + id);
        };

        vm.gotoContentMetadataEdit = function (id) {
            $location.path('/contentMetaEdit/' + id);
        };

        vm.gotoContentMetadataList = function () {
            $location.path('/contentMetaList');
        };
		
		
		/*----------------------------------------Get Content------------------------------------*/
        if (vm.currentContentId === undefined || vm.currentContentId === '') {
            //to retrieve all contents
            contentService.getAllContent()
                .then(function (success) {
                    vm.contentList = success.data;
                }, function (error) {

                });
        } else {
            //to retrieve particular content
            contentService.getContent(vm.currentContentId)
                .then(function (success) {
                    vm.content = success.data;
					console.log("vm.content=", vm.content);
				console.log("vm.content.tags=", vm.content.tags);

                    if (vm.content.cost == null || vm.content.cost == 'undefined') {
                        vm.content.cost = [];
                    }
                    if (vm.content.genre == null || vm.content.genre == 'undefined') {
                        vm.content.genre = [];
                    }
                    if (vm.content.languages == null || vm.content.languages == 'undefined') {
                        vm.content.languages = [];
                    }
                    if (vm.content.regionallang == null || vm.content.regionallang == 'undefined') {
                        vm.content.regionallang = [];
                    }
                    if (vm.content.targetAudience == null || vm.content.targetAudience == 'undefined') {
                        vm.content.targetAudience = [];
                    }
                    if (vm.content.targetRegion == null || vm.content.targetRegion == 'undefined') {
                        vm.content.targetRegion = [];
                    }
                    if (vm.content.distRightsOwner == null || vm.content.distRightsOwner == 'undefined') {
                        vm.content.distRightsOwner = [];
                    }
                    if (vm.content.omgProductList == null || vm.content.omgProductList == 'undefined') {
                        vm.content.omgProductList = [];
                    }
                    vm.content.launchMonthYear = dateFromISO8601(vm.content.launchMonthYear);
					vm.content.pubFrom = dateFromISO8601(vm.content.pubFrom);
					vm.content.pubTill = dateFromISO8601(vm.content.pubTill);
                    var ctn = 0;
                    angular.forEach(vm.content.cost, function () {
                        vm.content.cost[ctn].from = dateFromISO8601(vm.content.cost[ctn].from);
                        vm.content.cost[ctn].till = dateFromISO8601(vm.content.cost[ctn].till);
                        ctn = ctn + 1;
                    });
				
				/*contentService.getProductOptions()
				.then(function (success){
					vm.omgProductListOptions = success.data;
				});*/

                });
			
			
			 contentService.getProductOptions()
                .then(function (success) {
                     vm.omgProductListOptions = success.data;
			 });
			
        }
		/*---------------------------------------------------------------------------------------*/

        /*------------------Content Update function------------------------------*/
        vm.contentMetadataSaveBtn = function () {
            /*vm.contentFrm.submitted = true;
            if (vm.contentFrm.$valid) 
            {*/
            
            contentService.updateContent(vm.content)
                .then(function (success) {
				console.log("success.data=",success.data);
                    toaster.success({
                        title: "Success",
                        body: "Data Saved Successfully"
                    });
                }, function (error) {
                    toaster.error({
                        title: "Error",
                        body: "Error saving Data"
                    });
                });
            /*}
            else
            {
                toaster.error({
                            title: "Error",
                            body: "Some required information (*) is missing or incomplete."
                        });
              console.log("error"); 
            }*/

        };
		
		/*------------------------------------------------------------------------------*/
		
		/*---------------------------Content delete function----------------------------------*/
        vm.gotoContentMetadataDelete = function (id) {
            contentService.deleteContent(id)
                .then(function (success) {

                    for (var i = 0, len = vm.contentList.length; i < len; i++) {
                        if (vm.contentList[i].id == id) {
                            vm.contentList.splice(i, 1);
                        }
                    }
                    toaster.success({
                        title: "Success",
                        body: "Content Deleted Successfully"
                    });
                }, function (error) {
                    toaster.error({
                        title: "Error",
                        body: "Error.."
                    });
                });
        };
		/*------------------------------------------------------------------------------*/
		
		/*-----------------Date function-------------------------*/
        function dateFromISO8601(isostr) {
            if (typeof isostr != 'undefined' && isostr != null) {
                var parts = isostr.match(/\d+/g);
                return new Date(parts[0], parts[1] - 1, parts[2], parts[3], parts[4], parts[5]);
            }
        };
		/*------------------------------------------------------------------------------*/
		
		/*----------------------Upload Image function--------------------------------*/

        vm.upload = function (file, errFiles, fileType, fileSize, device, devWidth, imgWidth, imgHeight) {
            vm.f = file;
            vm.errFile = errFiles && errFiles[0];
			console.log("vm.errFile=",vm.errFile);
			console.log("file=",file);
            if (file) {
                file.upload = Upload.upload({
                    url: myConfig.apiUrl + '/api/content/UploadContentImage',
                    data: {
                        file: file,
                        'contentId': vm.currentContentId,
                        'imageType': fileType,
                        'resolution': fileSize,
						'deviceType': device,
						'deviceWidth': devWidth 
                    }
                });
                file.upload.then(function (response) {
                    $timeout(function () {
                        file.result = response.data;
                        console.log(file);
                        toaster.success({
                        title: "Success",
                        body: "File Uploaded Successfully"
                    });
                    });
                }, function (response) {
                    if (response.status > 0)
                        vm.errorMsg = response.status + ': ' + response.data;
					toaster.error({
                        title: "Error",
                        body: "File Not Uploaded Successfully"
                    });
                }, function (evt) {
                    file.progress = Math.min(100, parseInt(100.0 *
                        evt.loaded / evt.total));
                });
            } 
			
			else if (vm.errFile !== undefined && vm.errFile.$errorParam === ".jpg,.JPG") {
					 toaster.error({
                        title: "Error",
                        body: "Kindly upload a .jpg/.jpeg file"
                    });
			}
			else {
				     toaster.error({
                        title: "Error",
                        body: "Kindly upload an image file of size " + imgWidth + " * " + imgHeight
                    });
			}
        }
		/*------------------------------------------------------------------------------*/
		
		/*-----------------------------angularjs autocomplete--------------------------*/
		/*vm.source = function(param) {
			return $http.get(myConfig.apiUrl + '/api/actor/search?startsWith=' +param.keyword);
			//console.log("src fn",vm.content.director);
       };*/
		
		
		vm.data = {
                ActorId:"",
                ActorName: ""
                };
        
		/*vm.source1 = [];*/
		vm.source = function(param){
			console.log("param=",param);
			var deferred = $q.defer();
			contentService.getAutocompleteText(param.keyword)
                .then(function (data) {
				deferred.resolve(data);
                }, function (error) {
				console.log("error");
                });	
			console.log("deferred.promise=",deferred.promise);
			return deferred.promise;
		};
		
		
		/*-----------------------------angularjs autocomplete--------------------------*/
		
		/*vm.data = {
                actorId:"",
                actorName: ""
                };
        vm.source = [];
		
        castCrewService.getCCAllDetailsList()
                .then(function(data){
                      vm.autoData = data;
                      for(var i=0;i<=vm.autoData.length-1;i++)
                      {
                        vm.data.actorId = vm.autoData[i].id;
                        vm.data.actorName = vm.autoData[i].name;
                        vm.source.push({
                                           actorId: vm.data.actorId,
                                           actorName: vm.data.actorName
                                       });
                      }
                });*/
		/*----------------------------------------------*/
        
		/*vm.data = {
                actorId:"",
                actorName: ""
                };
        
		vm.source1 = [];
		vm.source = function(param){
			contentService.getAutocompleteText(param.keyword)
                .then(function (success) {
                  vm.autoData = success.data;
				vm.source1 = [];
				for(var i=0;i<=vm.autoData.length-1;i++)
                      {
                        vm.data.actorId = vm.autoData[i].id;
                        vm.data.actorName = vm.autoData[i].name;
                        vm.source1.push({
                                           actorId: vm.data.actorId,
                                           actorName: vm.data.actorName
                                       });
                      }
				console.log("vm.source1.length",vm.source1.length);
				if(vm.source1.length>=1){
					vm.source = [];
					vm.source = vm.source1;
				}
				
				console.log("source1 :",vm.source1);
				console.log("source :",vm.source);
                }, function (error) {
				console.log("error");

                });
						
		};*/
		
		
		
	
		
    
        /*-----------------------------------------------------------------------------*/
		
		
		/*--------------------------------Search functionality-------------------------*/
		
		vm.search = function(contentName, launchDate, genre, language, fromDate, toDate) {
			console.log("contentName=",contentName);
			console.log("launchDate=",launchDate);
			console.log("genre=",genre);
			console.log("language=",language);
			console.log("fromDate=",fromDate);
			console.log("toDate=",toDate);
		};
        
		/*--------------------------------Search functionality ends-------------------------*/

    };

    module.controller("contentMetadataController", contentMetadataController);

}(angular.module("app")));

/*});*/