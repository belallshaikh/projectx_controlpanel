(function (module) {
	module.controller("accountController", function ($routeParams, $location, toaster, accountService, cfpLoadingBar) {
		var vm = this;
		vm.search = {
			"by": "",
			"text": ""
		}

		vm.currentAId = $routeParams.id;
		

		
		vm.init = function () {

			vm.paymentData = [];
			if (vm.currentAId === undefined || vm.currentAId === '') {

				accountService.getAccountsList(vm.search)
					.then(function (data) {
						vm.accounts = data;
					console.log("accounts=", vm.accounts);
					
					});
			} else {
				accountService.getAccount(vm.currentAId)
					.then(function (data) {
						vm.account = data;
					console.log("account=",vm.account);
					accountService.getAccountProperties(vm.account.id)
					.then(function(success){
						vm.accountProperties=success.data;
						console.log("account Properties=",vm.accountProperties);
					})
					});
			}
		};

		vm.init();

/*--------------------------------Search functionality-------------------------*/
		vm.getAccounts = function () {

			accountService.getAccountsList(vm.search)
				.then(function (data) {
					console.log("data=", data);
					vm.accounts = data;
				});

		};
/*--------------------------------Search functionality ends-------------------------*/
		
/*--------------------------------Make Payout Functionality-------------------------*/		
		vm.makePayout = function () {
			console.log(vm.paymentData);
			accountService.makePayout(vm.paymentData)
				.then(function (response) {
					console.log(response);
				if(response.data==="fail")
					{
					alert("some payment can't be processed. Please check E-mail for more details");
					vm.init();
					}
				else{
					alert("payment done successfully");
					vm.init();
				}
				});

		};
/*--------------------------------Make Payout Functionality Ends-------------------------*/		

	
		vm.gotoAccountDetail = function (id) {
			console.log(id);
			$location.path("/AccountDetails/" + id);
		};

		vm.gotoPropertyDetail = function (id) {
			console.log(id);
			//$location.path("/AccountDetails/" + id);
		};

		vm.gotoAccountsList=function(){
			console.log("go back"); 
			$location.path('/AccountsList');
		}


		/*----------------------------------Reset functionality------------------------------*/

		vm.reset = function () {
			vm.search.by="";
			vm.search.text="";
		};

		/*----------------------------------Reset functionality ends-------------------------*/

	});

}(angular.module("app")));